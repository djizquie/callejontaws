package com.example.dnz.callejontaws;

/**
 * Created by dnz on 20/04/15.
 */
import android.app.Dialog;

import android.content.Context;

import android.hardware.Sensor;

import android.hardware.SensorEvent;

import android.hardware.SensorEventListener;

import android.hardware.SensorManager;

import android.os.Bundle;

import android.view.Surface;
import android.widget.TextView;



public class SensorDialog extends Dialog implements SensorEventListener {

    Sensor mSensor;
    //TextView mDataTxt;
    TextView dataTxtX;
    TextView dataTxtY;

    private SensorManager mSensorManager;
    Context ctx;



    public SensorDialog(Context context) {

        super(context);
        this.ctx=context;

        mSensorManager = (SensorManager)context.getSystemService(context.SENSOR_SERVICE);

        mSensor=mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

    }



    @Override

    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        //mDataTxt = (TextView) findViewById(R.id.sensorDataTxt);

        //mDataTxt.setText("...");

        dataTxtX=(TextView) findViewById(R.id.dataTxtX);
        dataTxtY=(TextView) findViewById(R.id.dataTxtY);
        dataTxtX.setText("...");
        dataTxtY.setText("...");
        setTitle(mSensor.getName());

    }



    @Override

    protected void onStart() {

        super.onStart();

        mSensorManager.registerListener(this, mSensor,  SensorManager.SENSOR_DELAY_FASTEST);

    }


    @Override

    protected void onStop() {

        super.onStop();

        mSensorManager.unregisterListener(this, mSensor);

    }



    @Override

    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }



    @Override

    public void onSensorChanged(SensorEvent event) {

        if (event.sensor.getType() != Sensor.TYPE_ACCELEROMETER) {

            return;

        }

        float accelX=0, accelY=0;


//detect the current rotation currentRotation from its “natural orientation”

//using the WindowManager
        int currentRotation = ((Activity)ctx).getWindowManager().getDefaultDisplay().getRotation();

        switch (currentRotation) {

            case Surface.ROTATION_0:

                accelX = event.values[0];

                accelY = event.values[1];
                break;
            case Surface.ROTATION_90:
                accelX = -event.values[0];
                accelY = event.values[1];
                break;
            case Surface.ROTATION_180:
                accelX = -event.values[0];
                accelY = -event.values[1];
                break;
            case Surface.ROTATION_270:
                accelX = event.values[0];
                accelY = -event.values[1];
                break;
        }

        dataTxtX.setText("" + accelX);
        dataTxtY.setText("" + accelY);
        //calculate the ball’s moving distances along x, and y using accelX, accelY and the time delta

    }


}

