package com.example.dnz.callejontaws;

import java.util.ArrayList;

import java.util.HashMap;

import java.util.List;

import java.util.Map;

import android.app.Fragment;

import android.content.Context;

import android.hardware.Sensor;

import android.hardware.SensorManager;

import android.os.Bundle;

import android.view.LayoutInflater;

import android.view.View;

import android.view.ViewGroup;

import android.widget.AdapterView;

import android.widget.AdapterView.OnItemClickListener;

import android.widget.ListView;

import android.widget.SimpleAdapter;



public class SensorInfoFragment extends Fragment {



    private View mContentView;



    private ListView mSensorInfoList;

    SimpleAdapter mSensorInfoListAdapter;



    private List<Sensor> mSensorList;



    private SensorManager mSensorManager;



    @Override

    public void onActivityCreated(Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);

    }



    @Override

    public void onPause()

    {

        super.onPause();

    }



    @Override

    public void onResume()

    {

        super.onResume();

    }



    @Override

    public View onCreateView(LayoutInflater inflater, ViewGroup container,

                             Bundle savedInstanceState) {

        mContentView = inflater.inflate(R.layout.content_sensorinfo_main, null);

        mContentView.setDrawingCacheEnabled(false);



        mSensorManager = (SensorManager)getActivity().getSystemService(Context.SENSOR_SERVICE);



        mSensorInfoList = (ListView)mContentView.findViewById(R.id.listSensorInfo);



        mSensorInfoList.setOnItemClickListener( new OnItemClickListener() {



            @Override

            public void onItemClick(AdapterView<?> arg0, View view, int index, long arg3) {



                // with the index, figure out what sensor was pressed

                Sensor sensor = mSensorList.get(index);



                // pass the sensor to the dialog.

                SensorDialog dialog = new SensorDialog(getActivity(), sensor);



                dialog.setContentView(R.layout.sensor_display);

                dialog.setTitle("Sensor Data");

                dialog.show();

            }

        });



        return mContentView;

    }



    void updateContent(int category, int position) {

        mSensorInfoListAdapter = new SimpleAdapter(getActivity(),

                getData() , android.R.layout.simple_list_item_2,

                new String[] {

                        "NAME",

                        "VALUE"

                },

                new int[] { android.R.id.text1, android.R.id.text2 });

        mSensorInfoList.setAdapter(mSensorInfoListAdapter);

    }





    protected void addItem(List<Map<String, String>> data, String name, String value)   {

        Map<String, String> temp = new HashMap<String, String>();

        temp.put("NAME", name);

        temp.put("VALUE", value);

        data.add(temp);

    }





    private List<? extends Map<String, ?>> getData() {

        List<Map<String, String>> myData = new ArrayList<Map<String, String>>();

        mSensorList = mSensorManager.getSensorList(Sensor.TYPE_ALL);



        for (Sensor sensor : mSensorList ) {

            addItem(myData, sensor.getName(),  "Vendor: " + sensor.getVendor() + ", min. delay: " + sensor.getMinDelay() +", power while in use: " + sensor.getPower() + "mA, maximum range: " + sensor.getMaximumRange() + ", resolution: " + sensor.getResolution());

        }

        return myData;

    }

}

